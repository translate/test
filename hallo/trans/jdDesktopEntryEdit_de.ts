<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>A graphical Program to create and edit desktop entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>The Icon was created by &lt;a href=&quot;https://icon-icons.com/icon/preferences-desktop-theme/93638&quot;&gt;Papirus Development Team&lt;/a&gt;</source>
        <translation>Das Icon wurde vom &lt;a href=&quot;https://icon-icons.com/icon/preferences-desktop-theme/93638&quot;&gt;Papirus Development Team&lt;/a&gt; erstellt</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>This Program is licensed under GPL 3</source>
        <translation>Dieses Programm ist unter der GPL 3 lizenziert</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>View source</source>
        <translation>Quelltext anzeigen</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="0"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>EditActionDialog</name>
    <message>
        <location filename="../EditActionDialog.py" line="35"/>
        <source>Identifier empty</source>
        <translation>Bezeichner leer</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.py" line="35"/>
        <source>The Identifier can&apos;t be empty</source>
        <translation>Der Bezeichner darf nicht leer sein</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.py" line="40"/>
        <source>Identifier exists</source>
        <translation>Bezeichner existiert</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.py" line="40"/>
        <source>This Identifier already exists</source>
        <translation>Dieser Bezeichner existiert bereits</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Identifier:</source>
        <translation>Bezeichner:</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Translate</source>
        <translation>Übersetzen</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Icon:</source>
        <translation>Icon:</translation>
    </message>
    <message>
        <location filename="../EditActionDialog.ui" line="0"/>
        <source>Exec:</source>
        <translation>Exec:</translation>
    </message>
</context>
<context>
    <name>EditKeywordsTranslationDialog</name>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="23"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="24"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="30"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="49"/>
        <source>No Language</source>
        <translation>Keine Sprache</translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="49"/>
        <source>You have to enter a Language</source>
        <translation>Du musst eine Sprache angeben</translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="54"/>
        <source>Language exists</source>
        <translation>Sprcahe existiert</translation>
    </message>
    <message>
        <location filename="../EditKeywordsTranslationDialog.py" line="54"/>
        <source>This Language already exists</source>
        <translation>Diese Sprache existiert bereits</translation>
    </message>
</context>
<context>
    <name>Functions</name>
    <message>
        <location filename="../Functions.py" line="124"/>
        <source>{{module}} not installed</source>
        <translation>{{module}} nicht installiert</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="124"/>
        <source>Optional module {{module}} not found. It is required to use this Feature.</source>
        <translation>Das optionale Modul {{module}} wurde nicht gefunden. Es wird zum benutzen dieses features benötigt.</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="135"/>
        <source>Invalid URL</source>
        <translation>Ungültige URL</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="135"/>
        <source>The given URL is not valid</source>
        <translation>Die angegebene URL ist ungültig</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="137"/>
        <source>Invalid Schema</source>
        <translation>Ungültiges Schema</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="137"/>
        <source>Only http and https are supported</source>
        <translation>Es werden nur http und https unterstützt</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="139"/>
        <source>Could not connect</source>
        <translation>Kann keine Verbindung aufbauen</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="139"/>
        <source>Could not connect to the Website</source>
        <translation>Kann nihct zur Webseite verbinden</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="141"/>
        <source>Unknown Error</source>
        <translation>Unbekannter Fehler</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="141"/>
        <source>An unknown Error happened while trying to conenct to the given URL</source>
        <translation>Während des verbindens zur URL ist ein unbekannter Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="144"/>
        <source>Could not get data</source>
        <translation>Kann keine Daten erhalten</translation>
    </message>
    <message>
        <location filename="../Functions.py" line="144"/>
        <source>Could not get data from the URL</source>
        <translation>Von der angegebenen URL können keine Daten erhalten werden</translation>
    </message>
</context>
<context>
    <name>Language</name>
    <message>
        <location filename="../SettingsDialog.py" line="17"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.py" line="18"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <location filename="../ListEditWidget.py" line="22"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="23"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="24"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="52"/>
        <source>Add Item</source>
        <translation>Item hinzufügen</translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="52"/>
        <source>Please enter a new Item</source>
        <translation>Bitte gib einen Text ein</translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="76"/>
        <location filename="../ListEditWidget.py" line="58"/>
        <source>Item in List</source>
        <translation>Item in der Liste</translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="76"/>
        <location filename="../ListEditWidget.py" line="58"/>
        <source>This Item is already in the List</source>
        <translation>Dieses Item ist bereits in der Liste vorhanden</translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="70"/>
        <source>Edit Item</source>
        <translation>Item bearbeiten</translation>
    </message>
    <message>
        <location filename="../ListEditWidget.py" line="70"/>
        <source>Please edit the Item</source>
        <translation>Bitte bearbeite das item</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.py" line="44"/>
        <source>A list of MimeTypes that this Application can open</source>
        <translation>Eine Liste der MimeTypen, die das programm öffnen kann</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="45"/>
        <source>The untranslated Keywords</source>
        <translation>Du unübersetzten Schlüsselwörter</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="46"/>
        <source>A list of interfaces that this application implements. If you don&apos;t know what this means, you probably won&apos;t need it.</source>
        <translation>Eine Lsite der Interfaces, die dieses programm implementiert. Wenn du nicht weisst wass es bedeutet, brauchst du es wahrscheinlich nicht.</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="47"/>
        <source>If set, the Application is only visble when using this Desktop Environments</source>
        <translation>Wenn gesetzt, wird der Desktopeintrag nur in diesen Desktopumgebungen angezeigt</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="48"/>
        <source>The Application is not visble when using this Desktop Environments</source>
        <translation>Dieser Desktopeintrag ist in diesen Desktopumgebungen nicht sichtbar</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="132"/>
        <source>Unsaved changes</source>
        <translation>Ungespeicherte Änderungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="132"/>
        <source>You have unsaved changes. Do you want to save now?</source>
        <translation>Du hast ungespeicherte Änderungen. Möchtest du jetzt speichern?</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="142"/>
        <source>Untitled</source>
        <translation>Unbenannt</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="155"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="170"/>
        <source>No templates found</source>
        <translation>Keine Vorlagen gefunden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="193"/>
        <source>No recent files</source>
        <translation>Keine zuletzt göffneten Dateien</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="206"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="244"/>
        <source>Open URL</source>
        <translation>URL öffnen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="244"/>
        <source>Please enter the URL to the Desktop Entry</source>
        <translation>Bitte gib die URL zum Desktopeintrag ein</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="273"/>
        <source>Add a Categorie</source>
        <translation>Kategorie hinzufügen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="273"/>
        <source>Please select a Categorie from the list below</source>
        <translation>Bitte wähle die Kategorie aus der Liste aus</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="277"/>
        <source>Categorie already added</source>
        <translation>Kategorie bereits vorhanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="277"/>
        <source>You can&apos;t add the same Categorie twice</source>
        <translation>Du kannst nicht die gleiche Kategorie zweimal hinzufügen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="319"/>
        <source>Delete Keywords translation</source>
        <translation>Schlüsselwörterübersetzung löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="319"/>
        <source>Are you really want to delete {{language}}?</source>
        <translation>Bist du sicher, dass du {{language}} löschen willst?</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="336"/>
        <source>Add Action</source>
        <translation>Aktion hinzufügen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="336"/>
        <source>Please enter the identifier for the new Action</source>
        <translation>Bitte gib den Bezeichner für die neue Aktion ein</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="342"/>
        <source>Identifier exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="342"/>
        <source>This Identifier already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="366"/>
        <source>Delete Action</source>
        <translation>Aktion löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="366"/>
        <source>Are you really want to delete {{identifier}}?</source>
        <translation>Bist du sicher, dass du {{identifier}} löschen möchtest?</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.py" line="391"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="410"/>
        <source>Invalid Key</source>
        <translation>Ungültiger Schlüssel</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="410"/>
        <source>{{key}} is not a valid custom Key</source>
        <translation>{{key}} ist kein gültiger Schlüssel</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="414"/>
        <source>Everything valid</source>
        <translation>Alles gültig</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="414"/>
        <source>No issues found</source>
        <translation>Es wurden keine Probleme gefunden</translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="451"/>
        <location filename="../MainWindow.py" line="426"/>
        <source>Error loading Desktop Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.py" line="451"/>
        <location filename="../MainWindow.py" line="426"/>
        <source>This Desktop Entry couldn&apos;t be loaded. Make sure, it is in the right format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Directory</source>
        <translation>Verzeichnis</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Translate</source>
        <translation>Übersetzen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>GenericName:</source>
        <translation>GenricName:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Icon:</source>
        <translation>Icon:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Exec:</source>
        <translation>Exec:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>TryExec:</source>
        <translation>TryExec:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Path:</source>
        <translation>Pfad:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>StartupWMClass:</source>
        <translation>StartupWMClass:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Don&apos;t display in Menu (NoDisplay)</source>
        <translation>Nicht im Menü anzeigen (NoDisplay)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Hide Desktop Entry (Hidden)</source>
        <translation>Desktopeintrag verstecken (Hidden)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Program has a single main window (SingleMainWindow)</source>
        <translation>Programm hat ein einzelnes Hauptfenster (SingleMainWindow)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Run on a more powerful discrete GPU if available (PrefersNonDefaultGPU)</source>
        <translation>Falls verfügbar auf einem eistungsstärkeren diskreten Grafikprozessor ausführen (PrefersNonDefualtGPU)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Send a Message when started (StartupNotify)</source>
        <translation>Das Programms sendet beim Starten eine Nachricht (StartupNotify)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Run in a Terminal (Terminal)</source>
        <translation>Im Terminal ausführen (Terminal)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Can be activated with D-Bus (DBusActivatable)</source>
        <translation>Kann mit D-Bus aktiviert werden (DBusActivatable)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Categories</source>
        <translation>Kategorien</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>MimeType</source>
        <translation>MimeType</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Keywords</source>
        <translation>Schlüsselwörter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Untranslated</source>
        <translation>Unübersetzt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Translated</source>
        <translation>Übersetzt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Actions</source>
        <translation>Aktionen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Implements</source>
        <translation>Implementiert</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>OnlyShowIn</source>
        <translation>OnlyShowIn</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>NotShowIn</source>
        <translation>NotShowIn</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Custom</source>
        <translation>Eigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Check</source>
        <translation>Überprüfen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open recent</source>
        <translation>Zuletzt geöffnete Dateien</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>New (from Template)</source>
        <translation>Neu (aus Vorlage)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Tools</source>
        <translation>Werkzeuge</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Save as...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Validate</source>
        <translation>Validieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Desktop Entry Specification</source>
        <translation>Desktopeintrag Spezifikation</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Manage templates</source>
        <translation>Vorlagen verwalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Open from URL</source>
        <translation>Von URL öffnen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Show Welcome Dialog</source>
        <translation>Willkommensdialog anzeigen</translation>
    </message>
</context>
<context>
    <name>ManageTemplatesWindow</name>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="67"/>
        <location filename="../ManageTemplatesWindow.py" line="46"/>
        <source>Enter name</source>
        <translation>Namen eingeben</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="46"/>
        <source>This will save your current document as template. Please enter a name.</source>
        <translation>Dies speichert dein aktuelles Dokument als Vorlage. Bitte gib einen Namen ein.</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="73"/>
        <location filename="../ManageTemplatesWindow.py" line="52"/>
        <source>Name exists</source>
        <translation>Name existiert</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="73"/>
        <location filename="../ManageTemplatesWindow.py" line="52"/>
        <source>There is already a template with this name</source>
        <translation>Es existiert bereits eine Vorlage mit diesem Namen</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="67"/>
        <source>Please enter the new name</source>
        <translation>Bitte gib einen neuen Namen ein</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="99"/>
        <location filename="../ManageTemplatesWindow.py" line="82"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="82"/>
        <source>A error occurred while renaming</source>
        <translation>Während des umbenennens ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="92"/>
        <source>Delete {{name}}</source>
        <translation>Lösche {{name}}</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="92"/>
        <source>Are you sure you want to delete {{name}}?</source>
        <translation>Bist du sicher, dass du {{name}} löschen möchtest?</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.py" line="99"/>
        <source>A error occurred while deleting</source>
        <translation>Während des Löschens ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Manage templates</source>
        <translation>Vorlagen verwalten</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Rename</source>
        <translation>Umbennen</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../ManageTemplatesWindow.ui" line="0"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>PreviewDialog</name>
    <message>
        <location filename="../PreviewDialog.ui" line="0"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../PreviewDialog.ui" line="0"/>
        <source>Preview of your desktop entry</source>
        <translation>Vorschau deines Desktop Eintrags</translation>
    </message>
    <message>
        <location filename="../PreviewDialog.ui" line="0"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../PreviewDialog.ui" line="0"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../SettingsDialog.py" line="31"/>
        <source>System language</source>
        <translation>Systemsprache</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.py" line="43"/>
        <source>Nothing</source>
        <translation>Nichts</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.py" line="44"/>
        <source>Filename</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.py" line="45"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>(needs restart)</source>
        <translation>(benötight Neustart)</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Length of recent files list:</source>
        <translation>Länge der Lsite der zuletzt geöffneten Dateien:</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Window title:</source>
        <translation>Fenstertitel:</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Ask before closing unsaved File</source>
        <translation>Vor dem schließen mit nicht gespeicherten Änderungen fragen</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Show in Title if File is edited</source>
        <translation>Im Fenstertitel anzeigen, wenn datei bearbeitet wurde</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Add comment to Files</source>
        <translation>Kommentar zu Dateien hinzufügen</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../SettingsDialog.ui" line="0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>TranslateDialog</name>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <location filename="../TranslateDialog.py" line="37"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.py" line="53"/>
        <source>No Language</source>
        <translation>Keine Sprache</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.py" line="53"/>
        <source>You had no Language for at least one Item</source>
        <translation>Mindestens eine Zeile hat keine Sprache</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.py" line="57"/>
        <source>Language double</source>
        <translation>Doppelte Sprache</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.py" line="57"/>
        <source>{{Language}} appears twice or more times in the table</source>
        <translation>{{language}} taucht mehrmals auf</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Translate</source>
        <translation>Übersetzen</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Translation</source>
        <translation>Übersetzung</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../TranslateDialog.ui" line="0"/>
        <source>Cancel</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>ValidationDialog</name>
    <message>
        <location filename="../ValidationDialog.py" line="23"/>
        <source>desktop-file-validate was not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ValidationDialog.ui" line="0"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ValidationDialog.ui" line="0"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WelcomeDialog</name>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>Welcome</source>
        <translation>Willkommen</translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>Welcome to jdDesktopEntryEdit!</source>
        <translation>Willkommen bei jdDesktopEntryEdit!</translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>This Program allows you to create and edit Desktop Entries according to the Freedesktop Specification.</source>
        <translation>Mit diesem Programm kannst du Desktop-Einträge gemäß der Freedesktop-Spezifikation erstellen und bearbeiten.</translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>Unlike other Programs, which try to make things easy by implementing only the main parts, the Goal of jdDesktopEntryEdit is to support the full specification. Thefore, it is expected to read the Specification before using this Program.</source>
        <translation>Im Gegensatz zu anderen Programmen, die versuchen, die Dinge einfach zu machen, indem sie nur die wichtigsten Teile implementieren, ist es das Ziel von jdDesktopEntryEdit, die gesamte Spezifikation zu unterstützen. Es wird daher erwartet, dass du die Spezifikation liest, bevor du das Programm verwendest.</translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>You can view the Specification at ?&gt;Destop Entry Specification.</source>
        <translation>Du kannnst dir die Spezifikation unter?&gt;Desktopeintrag Spezifikation ansehen.</translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>You can validate your Desktop Entry at Tools&gt;Validate.</source>
        <translation>Du kannst deinen Desktopeintrag unter Werkzeuge&gt;Validieren auf Gültigkeit testen.</translation>
    </message>
    <message>
        <location filename="../WelcomeDialog.ui" line="0"/>
        <source>Show this Dialog on Startup</source>
        <translation>Diesen Dialog beim Programmstart anzeigen</translation>
    </message>
</context>
</TS>
